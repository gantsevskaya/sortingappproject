package sortingAppEPAMTrainTests;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static sortingAppEPAMTrain.SortingInt.sortInt;

public class SortingIntTests {

    @Test
    public void testEmptyCase() {
        sortInt(new String[]{});
    }

    @Test
    public void testSingleElementArrayCase() {
        String[] actual = {"2"};
        String[] actual1 = {"-2"};
        String[] actual2 = {"0"};
        String[] actual3 = {"22222"};

        sortInt(actual);
        sortInt(actual1);
        sortInt(actual2);
        sortInt(actual3);

        assertEquals("2", actual[0]);
        assertEquals("-2", actual1[0]);
        assertEquals("0", actual2[0]);
        assertEquals("22222", actual3[0]);
    }

    @Test
    public void test10Arguments() {


        String[] actual = {"2", "3", "-1", "1", "5", "8", "-8", "2", "9", "1"};
        String[] actual1 = {"200", "31", "-10", "11", "5", "8", "-8", "2", "9", "1"};
        String[] actual2 = {"10", "6", "4", "2", "9", "7", "1", "8", "3", "5"};
        String[] actual3 = {"0", "11", "-11", "3", "-3", "14", "-14", "8", "-8", "-11"};

        String[] expected = {"-8", "-1", "1", "1", "2", "2", "3", "5", "8", "9"};
        String[] expected1 = {"-10", "-8", "1", "2", "5", "8", "9", "11", "31", "200"};
        String[] expected2 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        String[] expected3 = {"-14", "-11", "-11", "-8", "-3", "0", "3", "8", "11", "14"};

        assertArrayEquals(expected, sortInt(actual));
        assertArrayEquals(expected1, sortInt(actual1));
        assertArrayEquals(expected2, sortInt(actual2));
        assertArrayEquals(expected3, sortInt(actual3));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThan10Arguments() {
        String[] actual = {"5", "6", "3", "2", "1", "-1", "-2", "-10", "4", "10", "11", "3", "6", "7"};
        String[] actual1 = {"15", "14", "13", "12", "11", "10", "9", "8", "7", "6", "5", "4", "3", "2", "1", "0"};
        String[] actual2 = {"0", "1", "2", "0", "1", "2", "0", "1", "2", "0", "1", "2", "0", "1", "2", "0", "1", "2", "0", "1",
                "2", "0", "1", "2", "0", "1", "2", "0", "1", "2", "0", "1", "2", "0", "1", "2"};
        String[] actual3 = {"-5", "-6", "-3", "-2", "-1", "-1", "-2", "-10", "-4", "-10", "-11", "-3", "-6", "-7"};

        sortInt(actual);
        sortInt(actual1);
        sortInt(actual2);
        sortInt(actual3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonIntegerArguments() {
        String[] actual = {"check"};
        String[] actual1 = {"Hello", "world"};
        String[] actual2 = {"5.24", "3.11"};
        String[] actual3 = {"a", "b", "df", "lsjrgnvls"};
        sortInt(actual);
        sortInt(actual1);
        sortInt(actual2);
        sortInt(actual3);
    }
}

