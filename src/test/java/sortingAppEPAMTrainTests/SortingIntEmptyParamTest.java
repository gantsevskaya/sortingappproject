package sortingAppEPAMTrainTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sortingAppEPAMTrain.SortingInt;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingIntEmptyParamTest {
    private final String[] actual;

    public SortingIntEmptyParamTest(String[] actual) {
        this.actual = actual;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}}
        });
    }

    @Test
    public void testEmpty() {
        SortingInt.sortInt(actual);
        assertArrayEquals(new String[]{}, actual);
    }
}
