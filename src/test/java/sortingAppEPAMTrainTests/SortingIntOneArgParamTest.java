package sortingAppEPAMTrainTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sortingAppEPAMTrain.SortingInt;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingIntOneArgParamTest {

    private final String[] actual;
    private final String expected;

    public SortingIntOneArgParamTest(String[] actual, String expected) {
        this.actual = actual;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"0"}, "0"},
                {new String[]{"10"}, "10"},
                {new String[]{"100"}, "100"},
                {new String[]{"-10"}, "-10"},
                {new String[]{"333"}, "333"}
        });
    }

    @Test
    public void testNoRootsCase() {
        SortingInt.sortInt(actual);
        assertEquals(expected, actual[0]);
    }

}

