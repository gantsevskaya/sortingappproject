package sortingAppEPAMTrainTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sortingAppEPAMTrain.SortingInt;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingIntNonIntParamTest {
    private final String[] actual;

    public SortingIntNonIntParamTest(String[] actual) {
        this.actual = actual;
    }


    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new String[][][]{
                {{"-4", "a", "s", "d", "f", "a"}},
                {{"//2", "+", "res", "green", "yellow"}},
                {{"hello", "world", "java"}},
                {{"sky", " ", "blue"}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonIntArg() {
        SortingInt.sortInt(actual);
    }


}

