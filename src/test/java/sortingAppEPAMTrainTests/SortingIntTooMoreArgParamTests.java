package sortingAppEPAMTrainTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sortingAppEPAMTrain.SortingInt;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingIntTooMoreArgParamTests {
    private final String[] actual;

    public SortingIntTooMoreArgParamTests(String[] actual) {
        this.actual = actual;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"1", "2", "3", "4", "5", "-1", "-2", "-3", "-4", "-5", "1", "2", "3", "4", "5"}},
                {new String[]{"1", "2", "3", "4", "5", "-1", "-2", "-3", "-4", "-5", "1", "2", "3", "4"}},
                {new String[]{"1", "2", "3", "4", "5", "-1", "-2", "-3", "-4", "-5", "1", "2"}},
                {new String[]{"1", "2", "3", "4", "5", "-1", "-2", "-3", "-4", "-5", "1"}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooMoreArg() {
        SortingInt.sortInt(actual);
    }
}





