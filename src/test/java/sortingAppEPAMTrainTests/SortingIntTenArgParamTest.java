package sortingAppEPAMTrainTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sortingAppEPAMTrain.SortingInt;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingIntTenArgParamTest {

    private final String[] actual;
    private final String[] expected;

    public SortingIntTenArgParamTest(String[] actual, String[] expected) {
        this.actual = actual;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"2", "3", "-1", "1", "5", "8", "-8", "2", "9", "1"}, new String[]{"-8", "-1", "1", "1", "2", "2", "3", "5", "8", "9"}},
                {new String[]{"200", "31", "-10", "11", "5", "8", "-8", "2", "9", "1"}, new String[]{"-10", "-8", "1", "2", "5", "8", "9", "11", "31", "200"}},
                {new String[]{"10", "6", "4", "2", "9", "7", "1", "8", "3", "5"}, new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}},
                {new String[]{"0", "11", "-11", "3", "-3", "14", "-14", "8", "-8", "-11"}, new String[]{"-14", "-11", "-11", "-8", "-3", "0", "3", "8", "11", "14"}}
        });
    }

    @Test
    public void testTenArg() {
        assertArrayEquals(expected, SortingInt.sortInt(actual));
    }
}


