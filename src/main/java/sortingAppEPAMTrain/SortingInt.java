package sortingAppEPAMTrain;

import java.util.Arrays;
import java.util.regex.Pattern;

public class SortingInt {

    private static final Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    public static String[] sortInt(String[] strArray) {

        int[] intArray = new int[strArray.length];

        for (int i = 0; i < intArray.length; i++) {
            if (!isNumeric(strArray[i])) {
                throw new IllegalArgumentException("Should be integer");
            }
            intArray[i] = Integer.parseInt(strArray[i]);
        }

        if (intArray.length > 10) {
            throw new IllegalArgumentException("Input 10 or less integer");
        }
        String[] res = new String[intArray.length];
        Arrays.sort(intArray);
        for (int i = 0; i < res.length; i++) {
            res[i] = String.valueOf(intArray[i]);
        }
        return res;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }
}
